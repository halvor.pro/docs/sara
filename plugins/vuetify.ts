import type { ThemeDefinition } from "vuetify";
import { createVuetify } from "vuetify";
import { md3 } from "vuetify/blueprints";
import { VBtn, VContainer } from "vuetify/components";
import { VEmptyState } from "vuetify/labs/VEmptyState";
import { createVueI18nAdapter } from "vuetify/locale/adapters/vue-i18n";
import stroke from "@/iconsets/stroke";
import aliases from "@/iconsets";

const palette = {
  primary: "#7839EE",
  "on-primary": "#FBFAFF",

  secondary: "#2E90FA",
  "on-secondary": "#102A56",

  tertiary: "#15B79E",
  "on-tertiary": "#0A2926"
};

const light: ThemeDefinition = {
  dark: false,
  colors: {
    ...palette,
    background: "#FFFFFF",
    "on-background": "#101828",
    surface: "#F9FAFB",
    "on-surface": "#475467",
    error: "#F04438",
    "on-error": "#FFFFFF",
    warning: "#F79009",
    "on-warning": "#FFFFFF",
    success: "#17B26A",
    "on-success": "#FFFFFF",
    info: "#2970FF",
    "on-info": "#FFFFFF"
  }
};

const dark: ThemeDefinition = {
  dark: true,
  colors: {
    ...palette,
    background: "#111111",
    "on-background": "#F5F5F4",
    surface: "#202124",
    "on-surface": "#F8FAFC",
    error: "#F97066",
    "on-error": "#000000",
    warning: "#FEC84B",
    "on-warning": "#121212",
    info: "#528BFF",
    "on-info": "#000000"
  }
};

// noinspection JSUnusedGlobalSymbols
export default defineNuxtPlugin((app) => {
  const i18n = { global: app.$i18n };
  const vuetify = createVuetify({
    aliases: {
      VFab: VBtn,
      VPage: VContainer,
      VPageClean: VContainer
    },
    blueprint: md3,
    components: {
      VEmptyState
    },
    defaults: {
      VAlert: {
        variant: "tonal",
        rounded: "lg"
      },
      VAppBarNavIcon: {
        rounded: "xl"
      },
      VBtn: {
        color: "secondary",
        rounded: "lg",
        variant: "flat",
        exact: true,
        class: ["px-4"]
      },
      VCardActions: {
        class: "ma-2"
      },
      VCardTitle: {
        class: ["text-wrap"]
      },
      VCheckbox: {
        color: "secondary",
        density: "comfortable",
        hideDetails: true
      },
      VCheckboxBtn: {
        color: "secondary"
      },
      VChip: {
        rounded: "rounded"
      },
      VCol: {
        cols: 12
      },
      VEmptyState: {
        icon: "$warning"
      },
      VFab: {
        color: "primary",
        position: "fixed",
        location: "bottom end",
        class: ["mb-10", "mr-3"]
      },
      VForm: {
        validateOn: "blur"
      },
      VFileInput: {
        color: "primary",
        rounded: "lg",
        variant: "outlined",
        prependIcon: "",
        appendInnerIcon: "file"
      },
      VListSubheader: {
        class: ["text-uppercase", "font-weight-bold"]
      },
      VList: {
        color: "primary"
      },
      VListItem: {
        rounded: "lg"
      },
      VListItemTitle: {
        class: "font-weight-medium"
      },
      VTextarea: {
        variant: "outlined",
        rounded: "lg",
        color: "primary"
      },
      VTextField: {
        variant: "outlined",
        rounded: "lg",
        color: "primary"
      },
      VPage: {
        fluid: true,
        class: ["d-flex", "flex-grow-1", "flex-column", "rounded-xl"]
      },
      VPageClean: {
        fluid: true,
        class: ["d-flex", "flex-grow-1", "flex-column"]
      },
      VProgressCircular: {
        indeterminate: true,
        color: "secondary",
        ariaLabel: "$vuetify.loading"
      },
      VProgressLinear: {
        indeterminate: true,
        rounded: "rounded",
        color: "secondary",
        ariaLabel: "$vuetify.loading"
      },
      VSelect: {
        variant: "outlined",
        rounded: "lg",
        color: "primary",
        title: ""
      }
    },
    icons: {
      defaultSet: "stroke",
      aliases,
      sets: {
        stroke
      }
    },
    locale: {
      // @ts-expect-error - Adapter does not export types
      adapter: createVueI18nAdapter({ i18n, useI18n })
    },
    theme: {
      themes: { light, dark }
    },
    ssr: true
  });

  app.vueApp.use(vuetify);
});
