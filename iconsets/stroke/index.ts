import type { IconSet } from "vuetify";
import VIconStroke from "./VIconStroke.vue";

const stroke: IconSet = {
  component: VIconStroke
};

export default stroke;
