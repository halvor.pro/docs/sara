import { es } from "vuetify/locale";

// noinspection JSUnusedGlobalSymbols
export default defineI18nConfig(() => ({
  legacy: false,
  messages: { es: { $vuetify: es } },
  fallbackLocale: "es"
}));
