import vuetify from "vite-plugin-vuetify";

// https://nuxt.com/docs/api/configuration/nuxt-config
// noinspection JSUnusedGlobalSymbols
export default defineNuxtConfig({
  app: {
    head: {
      charset: "utf-8",
      viewport: "width=device-width, initial-scale=1.0",
      title: "Sara",
      meta: [
        {
          name: "og:title", content: "SARA"
        },
        {
          name: "twitter:title", content: "SARA"
        },
        {
          name: "description", content: "SARA es un sistema para las instituciones educativas que permite gestionar el registro académico de los estudiantes, además de brindar herramientas para diferentes procesos administrativos."
        },
        {
          name: "og:description", content: "SARA es un sistema para las instituciones educativas que permite gestionar el registro académico de los estudiantes, además de brindar herramientas para diferentes procesos administrativos."
        },
        {
          name: "twitter:description", content: "SARA es un sistema para las instituciones educativas que permite gestionar el registro académico de los estudiantes, además de brindar herramientas para diferentes procesos administrativos."
        },
        {
          name: "site_name", content: "SARA"
        },
        {
          name: "application-name", content: "SARA"
        },
        {
          name: "twitter:card", content: "summary"
        },
        {
          name: "twitter:site", content: "https://sara.halvor.pro"
        },
        {
          name: "twitter:image", content: "/android-chrome-512x512.png"
        },
        {
          name: "og:image", content: "/android-chrome-512x512.png"
        },
        {
          name: "author", content: "Halvor Stephen Peña Madrigal"
        },
        {
          name: "msapplication-TileColor", content: "#F9FAFB"
        },
        {
          name: "theme-color", content: "#F9FAFB", media: "(prefers-color-scheme: light)"
        },
        {
          name: "theme-color", content: "#202124", media: "(prefers-color-scheme: dark)"
        },
        {
          name: "background-color", content: "#F9FAFB", media: "(prefers-color-scheme: light)"
        },
        {
          name: "background-color", content: "#202124", media: "(prefers-color-scheme: dark)"
        },
        {
          name: "apple-mobile-web-app-status-bar-style", content: "black-translucent"
        }
      ],
      link: [
        {
          rel: "apple-touch-icon", sizes: "180x180", href: "/apple-touch-icon.png"
        },
        {
          rel: "icon", type: "image/png", sizes: "32x32", href: "/favicon-32x32.png"
        },
        {
          rel: "shortcut icon", href: "/favicon.ico"
        },
        {
          rel: "icon", type: "image/png", sizes: "16x16", href: "/favicon-16x16.png"
        },
        {
          rel: "manifest", href: "/site.webmanifest"
        },
        {
          rel: "mask-icon", href: "/safari-pinned-tab.svg", color: "#7839EE"
        }
      ]
    }
  },
  build: {
    transpile: ['vuetify']
  },
  components: [
    {
      path: "~/components",
      pathPrefix: false
    }
  ],
  css: [
    "~/assets/main.scss",
    "~/assets/styles.scss"
  ],
  devServer: {
    port: 3100
  },
  devtools: { enabled: true },
  modules: [
    (_options, nuxt) => {
      nuxt.hooks.hook("vite:extendConfig", (config) => {
        config.plugins?.push(vuetify({
          styles: {
            configFile: "assets/settings.scss"
          }
        }));
      });
    },
    "@vueuse/nuxt",
    "@nuxtjs/i18n",
    "@nuxtjs/sitemap",
    "@nuxtjs/robots",
    "@nuxtjs/google-fonts"
  ],
  googleFonts: {
    display: "swap",
    families: {
      Roboto: [100, 300, 400, 500, 700, 900]
    }
  },
  i18n: {
    locales: [{ code: "es", iso: "es" }],
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: "i18n_locale",
      redirectOn: "root"
    },
    defaultLocale: "es",
    strategy: "prefix_and_default"
  },
  nitro: {
    compressPublicAssets: true,
    prerender: {
      crawlLinks: true
    },
    static: true
  },
  site: {
    url: "https://sara.halvor.pro"
  },
  sourcemap: false
})
